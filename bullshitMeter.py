#!/usr/bin/python
# -*- coding: cp1252 -*-
#This script is just the transcription to Python language of PHP bullshitMeter
#but instead of english it measures the amount of bullshit in a spanish speech.
#-> I added some words in spanish too.
#h = "Pienso que una mentalidad perceptiva y amplios niveles de buena praxis pueden evitar un estancamiento del control de calidad pese a saber que soy dios"


import sys
import os
import xml.sax #Modulo de analisis sintactico de xml.
from xml.dom import minidom #Implementacion ligera del DOM
from xml.sax.handler import ContentHandler #Objeto manejador de contenido.
from optparse import OptionParser


def quitar_acentos(cadena):
   cadena.replace("á","a")
   cadena.replace("é","e")
   cadena.replace("í","i")
   cadena.replace("ó","o")
   cadena.replace("ú","u")
   return cadena


def cargar_xml():
   # Cargamos en objeto arbol_dom el documento xml
   arbol_dom = minidom.parse("palabras.xml")

   #Creo el objeto analizador sintactico y le asigno un manejador de contenido generico.
   analizador_xml = xml.sax.make_parser()
   analizador_xml.setContentHandler(ContentHandler())

   try:
      retUnicode = ""
      analizador_xml.parse("palabras.xml") #Compruebo que el xml este bien formado.
      nodos = arbol_dom.childNodes #Obtengo los nodos hijos del documento xml.
      for palabra in nodos[0].getElementsByTagName("palabra"):
         tag = palabra.toxml() #Convierto el objeto DOM en una linea xml.
         dato = tag.replace("<palabra>","").replace("</palabra>","") #Corto los tags por ambos lados.
         dato = dato.strip() #En caso de que haya espacios a algún lado de la palabra los corto.
         retUnicode = retUnicode + dato + " " #String separado por espacios
      retStr = retUnicode.encode() #Convierto de unicode a str ascii.
      return retStr.split() #Lista de palabras
   except Exception, err:
      print err


def bm_string(strg):
    chorradas = str(cargar_xml())
    string_aux = strg.lower() #Paso todo el string de entrada a minusculas.
    string_aux = quitar_acentos(string_aux)
    str_palabras = string_aux.split() #Me devuelve una lista de todas las palabras del string de entrada usando como separador el espacio en blanco.
    string_size = float(len(strg))

    c_aux = 0.0
    wtf  = 0.0

    for chorrada in chorradas:
        c_aux = 0.0
        for x in str_palabras:
            if (chorrada == x):
                c_aux += 1.0
        wtf += len(chorrada) * c_aux
    porcentaje =  (wtf / string_size) * 100

    return round(porcentaje,2)


def bm_file(fich):
	aux = ""
	with open(fich) as fichero:
		for linea in fichero:
			aux = aux + linea[:-1] + " " #Se elimina el carácter de salto de línea ('\n') y se añade un espacio, para darle continuidad al string.
	res = bm_string(aux)
	return res


def main():
	parser = OptionParser()
	parser.prog = "bullshitMeter.py"
	parser.add_option('-s', '--string',action='store',type='string', help="Cadena para analizar")
	parser.add_option('-f', '--file',action='store', type='string', help="Fichero para analizar")

	(opts, args) = parser.parse_args()

	if len(sys.argv) < 2:
		parser.print_help()
	else:
		if opts.string:
			sys.stdout.write("La cantidad de chorradas del string que has pasado por parámetro es: " + str(bm_string(opts.string)) + "%\n")
		if opts.file:
			if os.path.exists(opts.file) == False:
				sys.stderr.write("bullshitMeter.py: Error, el archivo que has especificado no existe\n")
			else:
				sys.stdout.write("La cantidad de chorradas del archivo que has pasado por parámetro es: " + str(bm_file(opts.file)) + "%\n")

main()
